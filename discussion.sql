-- To list down all the databases inside the DBMS:

SHOW DATABASES; -- Don't forget to include ; delimiters at the end of your codes. Also, the best practice here is to type your syntaxes in uppercase just to be able to differentiate them from database, table, and column names.

-- To let us create a database:
-- Syntax:
	-- CREATE DATABASE <name_db>;

CREATE DATABASE music_db;

-- DROP DATABASE = Deletion of database
-- Syntax:
	-- DROP DATABASE <name_db>;

DROP DATABASE music_db; -- Note that MySQL is not case sensitive. However, MySQL prevents users from creating multiple databases with the same name.

-- To SELECT a database:
-- Syntax
	-- USE <name_db>;

USE music_db;

-- CREATE Tables:
-- Table columns will also be declared here
CREATE TABLE users(
	-- INT indicates that the id column will have an integer entry.
	id INT NOT NULL AUTO_INCREMENT,
	-- VARCHAR indicates a string data type that also counts special characters; (50) indicates a character limit
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	-- CHAR does not count special characters (spaces, dashes, etc)
	contact_number CHAR(11) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50), -- Since this item will not be required of the user, we can leave out NOT NULL
	-- To set the primary key
	PRIMARY KEY (id)
);

-- Drop table from database
-- DROP TABLE <db_name>
DROP TABLE userss

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	playlist_name VARCHAR(30) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	-- CONSTRAINT indicates additional rules for the table
	CONSTRAINT fk_playlist_user_id -- Indicates that this additional rule is for the foreign key, followed by the table name and then the property
		FOREIGN KEY (user_id) REFERENCES users(id) -- REFERENCES table_name(table_column); indicates where the foreign key comes from
		ON UPDATE CASCADE -- If the user id changes, the change will also cascade and reflect in the playlist's information
		ON DELETE RESTRICT -- Prevents the source of the user_id from being deleted since the playlist table is dependent on the user
);

-- artist table
	
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)	
);

-- MINI-ACTIVITY for albums table
-- Include the columns: id, album_title, date_released, artist_id
	-- CASCADE
	-- Cannot delete artist
	-- Declare foreign key

CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	artist_id INT NOT NULL,
	album_title VARCHAR(50) NOT NULL,
	date_released DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- MINI-ACTIVITY for songs table
-- Columns: id, song_name, length, genre, album_id
	-- CASCADE
	-- Cannot delete album
	-- Declare fk

CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- MINI-ACTIVITY for playlists_songs
CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
    	ON DELETE RESTRICT
);
-- It's better to create multiple constraints per foreign key to ensure they register as foreign keys, and so that you can declare different additional rules for them, if there are any differences.